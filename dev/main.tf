module "vm" {
  source  = "../modules/vm"
  vm_name = "dev-vm-1"
}

output "ip" {
  value = module.vm.ip
}
