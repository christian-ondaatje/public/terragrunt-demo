# Generated by Terragrunt. Sig: nIlQXj57tbuaRZEa
terraform {
  backend "local" {}
}
variable "region" {}
provider "aws" {
  region = var.region
}
