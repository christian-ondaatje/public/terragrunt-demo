# Terragrunt Demo
### Christian Ondaatje


# development

1. install Terragrunt: `brew install terragrunt`

2. configure aws:

```
export AWS_ACCESS_KEY_ID="$(gpg --decrypt < "aws.secret" | sed -n 1p)"
export AWS_SECRET_ACCESS_KEY="$(gpg --decrypt < "aws.secret" | sed -n 2p)"
```

# usage

1. build all infra: `terragrunt run-all apply`

2. build dev infra: `terragrunt -chdir=./dev apply`

3. see all outputs: `terragrunt run-all output`



