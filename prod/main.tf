module "vm" {
  source  = "../modules/vm"
  vm_name = "prod-vm-1"
}

output "ip" {
  value = module.vm.ip
}
