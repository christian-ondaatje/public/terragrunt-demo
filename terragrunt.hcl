generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<-EOF
    terraform {
      backend "local" {}
    }
    variable "region" {}
    provider "aws" {
      region = var.region
    }
  EOF
}

terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()

    arguments = [
      "-var-file=../common.tfvars",
    ]
  }
}

remote_state {
  backend = "local"
  config = {
    path = "../secrets/${path_relative_to_include()}/terraform.tfstate"
  }
}
